<?php

namespace RESWUE\TrustTest;

use PHPUnit\Framework\TestCase;
use RESWUE\Trust\ExternalProfile;

class ExternalProfileTest extends TestCase
{
    public function testExternalProfile()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/external_profile.json'), true);

        $profile = new ExternalProfile($data);
        $this->assertEquals('https://www.example.com/user/908afb20-814f-58fa-a9ac-81719fe599f5', $profile->getUrl());
    }
}