<?php

namespace RESWUE\Trust\GroupMemberships;

class Membership
{
    /**
     * @var string
     */
    private $groupId;

    /**
     * @var string
     */
    private $state;

    public function __construct($data)
    {
        if (isset($data['group_id'])) {
            $this->setGroupId($data['group_id']);
        }

        if (isset($data['membership']['state'])) {
            $this->setState($data['membership']['state']);
        }
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     */
    public function setGroupId(string $groupId): void
    {
        $this->groupId = $groupId;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

}
